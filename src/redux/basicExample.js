import {createStore} from 'redux';

const {createStore} = Redux;

const initialState = {
  magazines: [],
  authors: []
}

const addMagazine = {type: 'ADD_MAGAZINE', magazine: 'Vouge'};

const addAuthor = {type:'ADD_Author', author:'Emma Specter'};

function myReducer(state = initialState, action){
  if(action.type=='ADD_MAGAZINE'){
    return{
      ...state,
      magazines: [...state.magazines , action.magazine]
    }
  }
  else if(action.type=='ADD_Author'){
    return{
      ...state,
      authors: [...state.authors , action.author]
    }
    }
}
  
const store = createStore(myReducer);

store.subscribe(()=>{

 console.log(store.getState());

});

store.dispatch(addAuthor)
