import {combineReducers} from 'redux';
const initialState = {
    
        categoryFilter: 'Fashion',
        articles: [
          {
            id: '1',
            title: 'Elections of USA 2020',
            author: 'Jacob Yang',
            category: 'Politics',
            article: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'
          },
          {
            id: '2',
            title: 'The right way to wear hats',
            author: 'Melisa Rendel',
            category : 'Fashion',
            article: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'
          },
          {
            id:'3',
            title: 'The right way to wear skirt',
            author: 'Barbare Jackson',
            category : 'Fashion',
            article: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'
          }
        ]
      
}


const articlesReducer = (state = initialState.articles , action) => {
    
  switch (action.type) {
      
      case 'DELETE_ARTICLE':

          let newArticles = state.filter(article => { return  article.id !== action.id } );

          return newArticles

      default:
         return state
    } 
}

const categoryReducer = (state = initialState.categoryFilter, action) => {
  switch (action.type) {
      case 'CHANGE_CATEGORY_FILTER':
          return action.categoryFilter
          
      default:
          return state
      
  }
}

const allReducers = combineReducers({

  categoryFilter: categoryReducer,
  articles: articlesReducer,
  
});

const rootReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CHANGE_CATEGORY_FILTER':
            return{
                ...state,
                categoryFilter : action.categoryFilter
            }
        
        case 'DELETE_ARTICLE':
            let newArticles = state.articles.filter(article => {
                return action.id !== article.id
            });
            return{
                ...state,
                articles : newArticles
            }
        default:
            return state
    }
    
}
export default rootReducer;

import categoryReducer from './categoryReducer';
import articlesReducer from './articlesReducer';
import {combineReducers} from 'redux';

const allReducers = combineReducers({
    categoryFilter: categoryReducer,
    articles: articlesReducer,
});
export default allReducers;



