
import initState from './initialState';


const articlesReducer = (arrayOfArticlesState = initState.articles , action) => {
    
    switch (action.type) {
        
        case 'DELETE_ARTICLE':
            let articles = arrayOfArticlesState.filter(article => { return  article.id !== action.id } );

            console.log('articlesReducer after delete',{
                articles:articles
            });

            return articles
           
        case 'ADD_ARTICLE':
            
            const newId = arrayOfArticlesState.length + 1+'';

            return [
                ...arrayOfArticlesState,
                {
                    id : newId,
                    title : action.newArticle.title,
                    author: action.newArticle.author,
                    category: action.newArticle.category,
                    article: action.newArticle.article
                }
            ]
            
        default:
            return arrayOfArticlesState
    }
    
}
export default articlesReducer;