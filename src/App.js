import React from 'react';
import {ThemeProvider} from './contexts/Context'
import ArticlesList from './Components/ArticlesList';

function App() {
  const theme = {
    isLightTheme: false,
    light:{
        syntax: '#555',
        ui:'#ddd',
        bg: '#f5f5dc'
    },
    dark:{
        syntax:'black',
        ui:'#fff',
        bg:'#f5f5dc'
    }
}
  return (

    <div className="App">
      <ThemeProvider value = {theme}>
        
          <ArticlesList/>
        
      </ThemeProvider>
    </div>
  );
}

export default App;
