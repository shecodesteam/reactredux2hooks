import React, { useState  ,useCallback  } from 'react';
import {useDispatch} from 'react-redux';
import {addArticle} from '../redux/actions/actions';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export const AddArticleForm = props => {
  const dispatch = useDispatch()
  
  const [title, setTitle] = useState('');
  const [author, setAuthor] = useState('');
  const [category, setCategory] = useState('');
  const [article, setArticle] = useState('');

  const addNewArticle = useCallback(
    (article) => dispatch(addArticle(article)),
    [dispatch]
  )


    const mySubmitHandler = event => {

        event.preventDefault();
        if(title==='' || author==='' || category === '' ||article==='' ){
          alert("Form wasn't completely filled! Try again!")
        }
        else{
          const articleToAdd = {
            title : title,
            author: author,
            category: category,
            article: article
          }
          addNewArticle(articleToAdd);
          setArticle('');
          setAuthor('');
          setCategory('');
          setTitle('');
        }

      }
      
     const selectChangeHandler = event =>{
       if(event.target.value.trim()===''){
         alert("Empty category!");
       }
       else{
        setCategory(event.target.value);
       }
        
      }
      const titleHandler = event => {
        if(event.target.value.trim()===''){
          alert("Empty title!");
        }
        else{
          setTitle(event.target.value);
        }
        
      }
      const authorHandler = event =>{
        setAuthor(event.target.value);
      }
      const articleHandler = event =>{
        setArticle(event.target.value);
      }
  
        return ( 
            <div>
            <Form onSubmit={mySubmitHandler}>
              <h1>Add new article:</h1>
              <Form.Label>Enter author:</Form.Label>
              <Form.Control type="text" name='author' placeholder="Enter author" onChange={authorHandler}/>
              <Form.Label>Enter title:</Form.Label>
              <Form.Control type="text" name='title'  placeholder="Enter title" onChange={titleHandler}/>
          
              <Form.Label>Select category:</Form.Label>
              <Form.Control as="select" onChange={selectChangeHandler}>
                <option value=""></option>
                <option value="Fashion">Fashion</option>
                <option value="Politics">Politics</option>
              </Form.Control>


              <Form.Label>Enter article:</Form.Label>
              <Form.Control type="text" name='article' placeholder="Enter article" onChange={articleHandler}/>

              <Button variant="outline-primary" type="submit">
                Submit
              </Button>
            </Form>
           </div>
         );
    
}

