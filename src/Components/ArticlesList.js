import React, {useContext ,useCallback } from 'react';
import ThemeContext  from '../contexts/Context';
import {AtricleInfo} from './ArticleInfo';
import {setCategoryFilter} from '../redux/actions/actions';
import {AddArticleForm} from './AddArticle';
import {useDispatch} from 'react-redux';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import Button from 'react-bootstrap/Button';


export const ArticlesList = props => {


  const articles = state => state.articles;
  const categoryFilter = useSelector(state => state.categoryFilter);

  const dispatch = useDispatch()

  const changeTheFilterCategory = useCallback(
    (newCategory) => dispatch(setCategoryFilter(newCategory)),
    [dispatch]
  )

  const selectNumOfDoneTodos = createSelector(
    articles,
    articles => articles.filter(article => article.category === categoryFilter)
  )
  
  const  themeContext  = useContext(ThemeContext);
 

  const changeFilterCategory=(filter, e) => {
      changeTheFilterCategory(filter);
    }
 
      
      
        const theme = themeContext.isLightTheme ? themeContext.light : themeContext.dark;
        const articlesByCategory = useSelector(selectNumOfDoneTodos)
      return ( 

        <div className="container" style={{color: theme.syntax, background:theme.ui}}>
          <div>
            <Button variant="outline-success" onClick = {(e) => changeFilterCategory("Politics", e)}>
                Politics
            </Button>
            <Button variant="outline-success" onClick = {(e) => changeFilterCategory("Fashion",e)}>
                Fashion
            </Button>

          </div>
          <h1  style={{color: '#555'}}>New articles about {categoryFilter}</h1>
          {
            articlesByCategory && articlesByCategory.length
              ? articlesByCategory.map((article, index) => {
                console.log(article);
                return (<AtricleInfo article={article} key={index} />);
              })
              : <p>No Articles</p>
          }
          <AddArticleForm/>
        </div>
      );
    
}

export default ArticlesList